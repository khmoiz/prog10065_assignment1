﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class Program
    {

        static void Main(string[] args)
        {

            List<Employee> Employees = new List<Employee>();
            double totalPR = 0.0;

            //VEHICLE//

            //Motorcycle : Vehicle
            Motorcycle suzuki = new Motorcycle("Suzuki GSX-R1000", "AB1 23C4", 1, "Midnight Blue");
            Motorcycle husq = new Motorcycle();
            husq.Make = "Husqvarna Vitpilen 401";
            husq.Plate = "23C 4AB1";
            husq.StorageBox = 1;
            husq.Color = "White";

            //Car : Vehicle
            Car nissan = new Car("Nissan GTR", "B12 3C4A", 2, "Silver");
            Car mclaren = new Car();
            mclaren.Make = "McLaren 570S";
            mclaren.Plate = "4C3 21BA";
            mclaren.Doors = 2;
            mclaren.Color = "Yellow";


            //EMPLOYEES//

            //Intern : Employee
            Intern in1 = new Intern("Moiz Khan", 20, "Sheridan College");
            Intern in2 = new Intern("Adrian Gonzalez Madruga", 18, "Sheridan College", nissan);
            Employees.Add(in1);
            Employees.Add(in2);


            //FullTime : Employee
            FullTime ft1 = new FullTime("Joe Ganczarski", 36, 100000, 9000);
            FullTime ft2 = new FullTime("Marc Bueno", 27, 200000, 15000, mclaren);
            Employees.Add(ft1);
            Employees.Add(ft2);

            //CommissionBasedPartTime : PartTime : Employee
            CommissionBasedPartTime cpt1 = new CommissionBasedPartTime("Noel Quinn", 31, 35, 20, 0.3, suzuki);
            CommissionBasedPartTime cpt2 = new CommissionBasedPartTime("Stephane Lemiuex", 28, 40, 25, 0.3);
            Employees.Add(cpt1);
            Employees.Add(cpt2);

            //FixedBasedPartTime : PartTime : Employee
            FixedBasedPartTime fpt1 = new FixedBasedPartTime("Khan Ali Moiz", 20, 21, 15, 150, nissan);
            FixedBasedPartTime fpt2 = new FixedBasedPartTime("Bueno Marc", 28, 30, 20, 99.99);
            Employees.Add(fpt1);
            Employees.Add(fpt2);


            //Display all information // OUTPUT

            Console.WriteLine("\t\t   MOIZ ALI KHAN | khmoiz | 991474771\n\n");
            Console.WriteLine("\t\t~~~~ -Employees Information List- ~~~~");
            Console.WriteLine("\t\t~~~~         Welcome...           ~~~~\n");

            foreach (Employee person in Employees)
            {

                Console.WriteLine(person.printMyData());
                totalPR = totalPR + person.calcEarnings();
                Console.WriteLine("________________________________________\n");
            }
            
            Console.WriteLine(String.Format("\t\t~~~~  -TOTAL PAYROLL: CAD {0:C}-  ~~~~ ",totalPR));
            Console.ReadKey();




        }    
    }

     
}
