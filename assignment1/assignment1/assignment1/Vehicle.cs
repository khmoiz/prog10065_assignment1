﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public abstract class Vehicle : IPrintable
    {
        private String _make;
        private String _plate;

        public Vehicle()
        {
            this._make = "";
            this._plate = "";
        }

        public Vehicle (String pMake, String pPlate)
        {
            _make = pMake;
            _plate = pPlate;
        }

        public string Plate { get => _plate; set => _plate = value; }
        public string Make { get => _make; set => _make = value; }

        public virtual String printMyData()
        {
            return (String.Format("\t-  Make: {0}\n\t-  Plate:{1}", Make, Plate));
        }
    }
}
