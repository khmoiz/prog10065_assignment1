﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class CommissionBasedPartTime : PartTime, IPrintable
    {
        //Priavte Variables
        private double _commissionPerc;
        

        //Constructors
        public CommissionBasedPartTime() : base()
        {
            this._commissionPerc = 0.0;

        }

        public CommissionBasedPartTime(String ppName, int ppAge, int pHoursWorked, int pRate, double pCommissionPerc) : base(ppName, ppAge, pHoursWorked, pRate)
        {
            this._commissionPerc = pCommissionPerc;

        }

        public CommissionBasedPartTime(String ppName, int ppAge, int pHoursWorked, int pRate, double pCommissionPerc, Vehicle pVehicle) : base(ppName, ppAge, pHoursWorked, pRate, pVehicle)
        {
            this._commissionPerc = pCommissionPerc;

        }


        //Getter methods for private variables
        public double CommissionPerc { get => _commissionPerc; set => _commissionPerc = value; }




        //Class methods
        public override double calcEarnings()
        {
            double fTotal = (base.HWorked * base.Rate);
            double commisions = fTotal * 0.2;    
            return (fTotal+commisions);
        }

        public override String printMyData()
        {
            String finalEarnings = String.Format("{0:C} ({1} + {2}% of {1})",calcEarnings(), (base.Rate*base.HWorked), 0.2*100 );
            return (String.Format("{0}\nEmployee is PartTime / Commissioned\n\t-  Rate: {1:C}\n\t-  Hours Worked: {2}\n\t-  Commission: {3}\n\t-  Earnings: {4}",base.printMyData(),base.Rate, base.HWorked, CommissionPerc,finalEarnings));
        }


       


       
    }
}
