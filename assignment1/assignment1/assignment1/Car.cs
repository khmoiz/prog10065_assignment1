﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class Car: Vehicle, IPrintable
    {
        private int _doors;
        private String _color;

        public Car() : base()
        {
            this._doors = 0;
            this._color = "";
        }

        public Car(String pMake, String pPlate, int pDoor, String pColor) : base(pMake, pPlate)
        {
            _doors = pDoor;
            _color = pColor;
        }

        public int Doors { get => _doors; set => _doors = value; }
        public string Color { get => _color; set => _color = value; }

        public override String printMyData()
        {
            return (String.Format("{0}\n\t-  Doors: {1}\n\t-  Color: {2}", base.printMyData(),Doors.ToString(), Color));
        }
    }
}
