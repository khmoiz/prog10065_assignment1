﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public abstract class  Employee : IPrintable
    {
        private String _name;
        private int _age;
        private Vehicle _vehicle;


        //CONSTRUCTORS
        public Employee()
        {
            this._name = "";
            this._age = 0;
            this._vehicle = null;
        }

        public Employee(String pName, int pAge)
        {
            this._name = pName;
            this._age = pAge;
            this._vehicle = null;
        }

        public Employee(String pName, int pAge, Vehicle pVehicle)
        {
            this._name = pName;
            this._age = pAge;
            this._vehicle = pVehicle;
        }


        //Getters for private variables
        public string Name
        {
            get
            {
                return _name;
            }
        }
        
        public int Age
        {
            get
            {
                return _age;
            }

            set
            {
                if (value >= 0) {
                    _age = value;
                } else
                {
                    _age = 0;
                }
                
            }
        }

        private Vehicle Vehicle { get => _vehicle; set => _vehicle = value; }
       

        //Methods
        public int calcBirthYear()
        {
            return (2018 - Age);
        }

        public abstract double calcEarnings();
       
        public virtual String printMyData()
        {
            String vehicleMessage;
            if (Vehicle == null)
            {
                vehicleMessage = "Employee has no registered Vehicle";
            }else{
                vehicleMessage = Vehicle.printMyData();
            }
            
            return(String.Format("Name: {0}\nYear of Birth: {1}\n{2}",Name,calcBirthYear().ToString(),vehicleMessage));
   
        }
    }
}
