﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class Intern : Employee, IPrintable
    {
        //Private Variables
        private String _schoolName;

        //Class Constructors
        public Intern () : base()
        {
            _schoolName = "";

        }

        public Intern(String pName, int pAge, String pSchoolName) : base(pName, pAge)
        {
            _schoolName = pSchoolName;
        }

        public Intern(String pName, int pAge, String pSchoolName, Vehicle pVehicle) : base(pName, pAge, pVehicle)
        {
            _schoolName = pSchoolName;
        }


        //Variable Getter and Setter
        public string SchoolName { get => _schoolName; set => _schoolName = value; }


        //Class Methods
        public override double calcEarnings()
        {
            return 2000.00;
        }

        public override String printMyData()
        {

            return (String.Format("{0}\nEmployee is Intern\n\t-  School Name: {1}\n\t-  Earnings: {2:C}", base.printMyData(), SchoolName, calcEarnings()));

        }


    }
}
