﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class FixedBasedPartTime: PartTime, IPrintable
    {
        //Priavte Variables
        private double _fixedAmount;


        //Constructors
        public FixedBasedPartTime() : base()
        {
            this._fixedAmount = 0.0;

        }

        public FixedBasedPartTime(String ppName, int ppAge, int pHoursWorked, int pRate, double pfixedAmount) : base(ppName, ppAge, pHoursWorked, pRate)
        {
            this._fixedAmount = pfixedAmount;

        }

        public FixedBasedPartTime(String ppName, int ppAge, int pHoursWorked, int pRate, double pfixedAmount, Vehicle pVehicle) : base(ppName, ppAge, pHoursWorked, pRate, pVehicle)
        {
            this._fixedAmount = pfixedAmount;

        }


        //Getter methods for private variables
        public double FixedAmount { get => _fixedAmount; set => _fixedAmount = value; }




        //Class methods
        public override double calcEarnings()
        {
            double fTotal = (base.HWorked * base.Rate);
            return (fTotal + FixedAmount);
        }

        public override String printMyData()
        {
            String finalEarnings = String.Format("{0:C} ({1} + {2})", calcEarnings(), (base.Rate * base.HWorked), FixedAmount);
            return (String.Format("{0}\nEmployee is PartTime / Fixed Amt\n\t-  Rate: {1:C}\n\t-  Hours Worked: {2}\n\t-  Fixed: {3:C}\n\t-  Earnings: {4:C}", base.printMyData(), base.Rate, base.HWorked, FixedAmount, finalEarnings));
        }


    }
}
