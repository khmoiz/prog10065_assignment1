﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public abstract class PartTime : Employee, IPrintable
    {
        //Priavte Variables
        private int _hWorked;
        private int _rate;
        
        //Constructors
        public PartTime() : base()
        {
            this._hWorked = 0;
            this._rate = 0;

        }

        public PartTime(String ppName, int ppAge, int pHoursWorked, int pRate) : base(ppName, ppAge)
        {
            this._hWorked = pHoursWorked;
            this._rate = pRate;
            
        }

        public PartTime(String ppName, int ppAge, int pHoursWorked, int pRate, Vehicle pVehicle) : base(ppName, ppAge, pVehicle)
        {
            this._hWorked = pHoursWorked;
            this._rate = pRate;
        }


        //Getter Methods
        public int HWorked { get => _hWorked; set => _hWorked = value; }
        public int Rate { get => _rate; set => _rate = value; }



        //Class methods
        public override double calcEarnings()
        {
            return _hWorked * _rate;
        }

        public override String printMyData()
        {
            return(base.printMyData());
        }


      
    }
}
