﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    class Motorcycle: Vehicle, IPrintable
    {
        private int _storageBox;
        private String _color;

        public Motorcycle() : base()
        {
            this._storageBox = 0;
            this._color = "";
        }

        public Motorcycle(String pMake, String pPlate, int pstorageBox, String pColor) : base(pMake, pPlate)
        {
            _storageBox = pstorageBox;
            _color = pColor;
        }

        public int StorageBox { get => _storageBox; set => _storageBox = value; }
        public string Color { get => _color; set => _color = value; }

        public override String printMyData()
        {
            return (String.Format("{0}\n\t-  Storage Boxes: {1}\n\t-  Color: {2}", base.printMyData(), StorageBox.ToString(),Color));
        }
    }
}
