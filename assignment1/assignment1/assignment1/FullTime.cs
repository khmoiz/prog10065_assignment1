﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment1
{
    public class FullTime : Employee, IPrintable
    {
        // class variables
        private int _salary;
        private int _bonus;
        private Vehicle _vehicle;


        //Class constructors
        public FullTime() : base()
        {
            this._salary = 0;
            this._bonus = 0;

        }

        public FullTime(String ppName, int ppAge, int pSalary, int pBonus, Vehicle pVehicle) : base(ppName, ppAge, pVehicle)
        {
            this._salary = pSalary;
            this._bonus = pBonus;
        }

        public FullTime(String ppName, int ppAge, int pSalary, int pBonus) : base(ppName, ppAge)
        {
            this._salary = pSalary;
            this._bonus = pBonus;
        }


        //Variable getters
        public int Salary
        {
            get
            {
                return _salary;
            }

        }

        public int Bonus
        {
            get
            {
                return _bonus;
            }

        }

        public Vehicle Vehicle { get => _vehicle; set => _vehicle = value; }



        //Class Methods


        public override double calcEarnings()
        {
            return _salary + _bonus;
        }

        public override String printMyData()
        {
            
            return (String.Format("{0}\nEmployee is FullTime\n\t-  Salary: {1:C}\n\t-  Bonus: {2:C}\n\t-  Earnings: {3:C}",base.printMyData(), Salary, Bonus,calcEarnings()));

        }



    }
}
